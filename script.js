let book, chapter, verse;

getBook();

function getBook() {
  // document.getElementById("get-book").innerHTML = "";
  axios({
    method: "GET",
    url: "https://getbible.net/v2/kjv.json",
    dataType: "jsonp",
    jsonp: "getbible",
  }).then(function (response) {
    var output = "";
    book = Math.floor(Math.random(0) * response.data.books.length);
    chapter = Math.floor(
      Math.random(0) * response.data.books[book].chapters.length
    );
    verse = Math.floor(
      Math.random(0) * response.data.books[book].chapters[chapter].verses.length
    );
    output +=
      "<center><b>" +
      response.data.books[book].chapters[chapter].verses[verse].name +
      "</b></center> ";

    output +=
      '<p>  <small class="ltr">' +
      response.data.books[book].chapters[chapter].verses[verse].text +
      "</small>  ";
    output += "<br/>";
    output += "</p>";
    jQuery("#scripture").html(output);
  });
}
